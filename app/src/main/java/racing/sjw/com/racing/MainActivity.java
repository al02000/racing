package racing.sjw.com.racing;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.tnkfactory.ad.BannerAdListener;
import com.tnkfactory.ad.BannerAdView;
import com.tnkfactory.ad.TnkAdListener;
import com.tnkfactory.ad.TnkSession;

public class MainActivity extends AppCompatActivity {

    private static final int INIT_SPEED = 12;
    private static final int SPEED_INTERVAL = 2;
    private static final int MAX_SPEED = 40;
    private static final int PHONE_STATE = 11;

    private View contLabel;
    private TextView tvNotify;
    private TextView tvScore, tvLevel, tvBest;
    private ImageView ivCenter;
    private RacingView racingView;
    private int score, level, bestScore;
    private int playCount;
    private BannerAdView bannerAdView;
    private FirebaseAnalytics mFirebaseAnalytics;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        requestNewInterstitial();
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "testItem");
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "TestItemName");
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "Test Content Type");
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);

        contLabel = findViewById(R.id.contNotify);
        tvNotify = (TextView) findViewById(R.id.notify);

        tvScore = (TextView) findViewById(R.id.score);
        tvLevel = (TextView) findViewById(R.id.level);
        tvBest = (TextView) findViewById(R.id.best);

        ivCenter = (ImageView) findViewById(R.id.imgCenter);
        ivCenter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                play();
            }
        });

        racingView = (RacingView) findViewById(R.id.racingView);
        playCount = 0;
        initialize();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (racingView != null && racingView.getPlayState() == RacingView.PlayState.Pause) {
            racingView.resume();
        }
        if (bannerAdView != null) {
            bannerAdView.onResume();
        }
    }

    @Override
    protected void onPause() {
        if (racingView != null && racingView.getPlayState() == RacingView.PlayState.Playing) {
            pause();
        }

        if (bannerAdView != null) {
            bannerAdView.onPause();
        }
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        racingView.reset();
        super.onDestroy();
    }

    private Handler racingHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case RacingView.MSG_SCORE:
                    score = score + (level);
                    tvScore.setText(String.valueOf(score));
                    break;

                case RacingView.MSG_COLLISION:
                    boolean achieveBest = false;
                    if (bestScore < score) {
                        tvBest.setText(String.valueOf(score));
                        bestScore = score;
                        saveBestScore(bestScore);
                        achieveBest = true;
                    }
                    collision(achieveBest);
                    break;

                case RacingView.MSG_COMPLETE:
                    level++;
                    if (racingView.getSpeed() < MAX_SPEED) {
                        racingView.setSpeed(racingView.getSpeed() + SPEED_INTERVAL);
                    }
                    tvLevel.setText(String.valueOf(level));
                    prepare();
                    break;
                default:
                    break;
            }
        }
    };

    private void initialize() {
        reset();
        prepare();
    }

    private int loadBestScore() {
        SharedPreferences preferences = getSharedPreferences("MyFirstGame", Context.MODE_PRIVATE);
        if (preferences.contains("BestScore")) {
            return preferences.getInt("BestScore", 0);
        } else {
            return 0;
        }
    }

    private void saveBestScore(int bestScore) {
        SharedPreferences preferences = getSharedPreferences("MyFirstGame", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt("BestScore", bestScore);
        editor.commit();
    }

    private void reset() {
        score = 0;
        level = 1;
        bestScore = loadBestScore();
        racingView.setSpeed(INIT_SPEED);
        racingView.setPlayState(RacingView.PlayState.Ready);

        tvScore.setText(String.valueOf(score));
        tvLevel.setText(String.valueOf(level));
        tvBest.setText(String.valueOf(bestScore));
    }

    private void prepare() {
        tvLevel.setText(String.valueOf(level));
        tvNotify.setText("LEVEL " + level);
        showLabelContainer();

        ivCenter.setImageResource(R.drawable.ic_play);
    }

    private void play() {
        if (racingView.getPlayState() == RacingView.PlayState.Collision) {
            initialize();

            racingView.reset();
            return;
        }

        if (racingView.getPlayState() == RacingView.PlayState.Playing) {
            pause();
        } else {
            ivCenter.setImageResource(R.drawable.ic_pause);

            showArrowToast();

            if (racingView.getPlayState() == RacingView.PlayState.Pause) {
                racingView.resume();
            } else if (racingView.getPlayState() == RacingView.PlayState.LevelUp) {
                racingView.resume();
                hideLabelContainer();
            } else {
                playCount++;
                if (playCount > 5) {
                    playCount = 0;

                }

                hideLabelContainer();
                racingView.play(racingHandler);
            }
        }
    }

    private void pause() {
        ivCenter.setImageResource(R.drawable.ic_play);
        racingView.pause();
    }

    private void collision(boolean achieveBest) {
        if (achieveBest) {
            tvNotify.setText("Congratulation!\nYou are the Best!");
        } else {
            tvNotify.setText("Try again!!");
        }

        contLabel.setVisibility(View.VISIBLE);
        contLabel.startAnimation(AnimationUtils.loadAnimation(this, android.R.anim.slide_in_left));

        ivCenter.setImageResource(R.drawable.ic_retry);
    }

    private void showLabelContainer() {
        contLabel.setVisibility(View.VISIBLE);
        contLabel.startAnimation(AnimationUtils.loadAnimation(this, android.R.anim.slide_in_left));
    }

    private void hideLabelContainer() {
        Animation anim = AnimationUtils.loadAnimation(this, android.R.anim.slide_out_right);
        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                contLabel.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        contLabel.startAnimation(anim);
    }

    private void showArrowToast() {
        final View v = findViewById(R.id.toast);
        Animation anim = AnimationUtils.loadAnimation(this, android.R.anim.fade_in);
        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                v.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                v.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        hideArrowToast();
                    }
                }, 1000);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        v.startAnimation(anim);
    }

    private void hideArrowToast() {
        final View v = findViewById(R.id.toast);
        Animation anim = AnimationUtils.loadAnimation(this, android.R.anim.fade_out);
        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                v.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        v.startAnimation(anim);
    }

    @Override
    public void onBackPressed() {
        showExitInterstitial();
    }

    private void showExitInterstitial(){
        TnkSession.prepareInterstitialAd(this, TnkSession.CPC);
        TnkSession.showInterstitialAd(this,  new TnkAdListener() {

            @Override
            public void onClose(int type) {
                if(type == TnkAdListener.CLOSE_EXIT)
                    MainActivity.this.finish();
            }

            @Override
            public void onShow() {
                Log.i("TEST", "onShow");
            }

            @Override
            public void onFailure(int i) {
                Log.i("TEST", "onFail");
                MainActivity.this.finish();
            }

            @Override
            public void onLoad() {
                Log.i("TEST", "onLoad");
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @Nullable String[] permissions, @Nullable int[] grantResult){
        switch (requestCode){
            case PHONE_STATE:
                if(grantResult.length > 0 && grantResult[0] != PackageManager.PERMISSION_GRANTED){
                    MainActivity.this.finish();
                }
                break;
        }
    }

    private void requestNewInterstitial() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, PHONE_STATE);
        }else{
            TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            Log.i("TEST", "DeviceId => "+tm.getDeviceId());
            TnkSession.setUserName(this, tm.getDeviceId());
        }
        bannerAdView = (BannerAdView)findViewById(R.id.banner_id);
        bannerAdView.setBannerAdListener(new BannerAdListener() {
            @Override
            public void onFailure(int i) {

            }

            @Override
            public void onShow() {

            }

            @Override
            public void onClick() {

            }
        });

        bannerAdView.loadAd(TnkSession.CPC);
    }
}
